var title = $("#title");
var imgSrc = $('#imgSrc');
var photo = $("#catPhoto");
var btn = $("#mainBtn");
var advPn = $("#advancedPanel");
var exLnk = $("#expandLink");
var panel = $("#panel");
var cates = $("#cates");
var breds = $('#breds');
var container = $("#container");
var searchURL = "https://api.thecatapi.com/v1/images/search";
var bred;
var cate;

/*
This "document.ready" function loads the category list and inserts the relevant
data into the first "select" element in the Advanced Options Panel.
*/
$(document).ready(function () {
  $.ajax({
    url: "https://api.thecatapi.com/v1/categories?api_key=f1760324-a551-467f-8d2b-4fb409cb1c09"
  }).then(function (data) {
    var count = 0;
    while (count < data.length) {
      var o = document.createElement("OPTION");
      o.append(data[count].name);
      o.setAttribute("value", data[count].id);
      cates.append(o);
      count++;
    }
  });
  $.ajax({
    url: "https://api.thecatapi.com/v1/breeds?api_key=f1760324-a551-467f-8d2b-4fb409cb1c09"
  }).then(function (data) {
    var count = 0;
    while (count < data.length) {
      var o = document.createElement("OPTION");
      o.append(data[count].name);
      o.setAttribute("value", data[count].id);
      breds.append(o);
      count++;
    }
  });
});

breds.change(function () {
  searchURL = "https://api.thecatapi.com/v1/images/search";
  bred = "?breed_ids=" + breds.val();
  searchURL = searchURL + bred;
  cates.val("0").attr('selected', 'selected');
});

cates.change(function () {
  searchURL = "https://api.thecatapi.com/v1/images/search";
  cate = "?category_ids=" + cates.val();
  searchURL = searchURL + cate;
  breds.val("0").attr('selected', 'selected');
});

/*
The "loadCat" function loads a random image from TheCatApi and displays it in
the right box, adds a link to the source of the image and displays the image id.
*/
function loadCat() {
  btn.addClass("is-loading");
  container.addClass("is-horizontal-center");
  container.addClass("is-flex");
  $.ajax({
    url: searchURL,
    type: "GET",
    dataType: "json",
    headers: { "x-api-key": "f1760324-a551-467f-8d2b-4fb409cb1c09" }
  }).then(function (data) {
    photo.attr("src", data[0].url);
    title.empty();
    title.append("Image ID: " + data[0].id);
    title.fadeIn("slow", "swing");
    imgSrc.attr("href", data[0].url);
    imgSrc.fadeIn();
    btn.removeClass("is-loading");
  });
}

/*
The "expand" functions expands the Advanced Options Panel and hides the expands
link.
*/
function expand() {
  panel.animate({
    height: '+=185px'
  });
  advPn.fadeIn("fast", "swing");
  exLnk.fadeOut("fast", "swing");
}


/*
The "contract" function collapses the Advanced Optios Panel and shows the expand
link
*/
function collapse() {
  searchURL = "https://api.thecatapi.com/v1/images/search";
  console.log(searchURL);
  advPn.fadeOut("fast", "swing");
  exLnk.fadeIn("fast", "swing");
  panel.animate({
    height: '-=185px'
  });
  cates.val("0").attr('selected', 'selected');
  breds.val("0").attr('selected', 'selected');
}
